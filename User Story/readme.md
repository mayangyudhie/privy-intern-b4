# B4 - Banking App User Story :

1. **As a Customer**
I want to Login to my account using card and PIN code. So that I can perform the transactions.
**Acceptance Criteria** :
• System must validate the card and pin code.
• In case Customer enters wrong Pin code three times then the system locks the card.
2. **As a Customer**
I want to to check the balance of my bank account. So that I can perform transactions.
**Acceptance Criteria :**
• Customer needs to be logged in before checking balance.
• Balances is displayed.
3. **As a Customer**
I want to to deposit cash in my bank account through ATM. So that I may save my time and perform transactions later.
**Acceptance Criteria :**
• Customer needs to be logged in before depositing cash.
• System should verify the amount of cash deposited by checking with the user.
• If the user doesn’t agree then the system ejects back the cash.
• If Ok the account balance is updated and displayed.
4. **As a Customer**
I want to to deposit check in my bank account through ATM. So that I may save my time and perform transactions later.
**Acceptance Criteria :**
• Customer needs to be logged in before depositing check.
• System should verify the amount written on the deposited check by asking the user.
• If the user doesn’t agree then the system ejects back the check.
• If Ok the account balance is updated and displayed.
5. **As a Customer**
I want to withdraw cash from my bank account through ATM. So that I may save my time.
**Acceptance Criteria :**
• Customer needs to be logged in before withdrawing cash.
• System checks to see if the request amount exceeds the balance.
• If so the system displays the balance and asks the user to enter a new amount.
• If amount entered is less than the account balance cash is dispensed and the new balance is displayed.
6. **As a Customer**
I want to transfer money from my account to another bank account through ATM. So that I may save my time.
**Acceptance Criteria :**
• Customer needs to be logged in before transferring amount.
• System should check the receivers account number and validate it prior to performing
the transactions.
• If Ok the local account balance is updated and displayed.
• System should update both accounts concurrently.
7. **As a Customer**
I want to logout from my bank account through ATM. So that I may end up my ATM session.
**Acceptance Criteria :**
• System asks user if the user wants session report and receipt for the entire session.
• If yes then the receipt is dispensed.
• User is logged off from the account.

https://www.notion.so/B4-Banking-App-User-Story-a0961915425c4a1a98e89e4153a77351